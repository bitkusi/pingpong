'use strict';

/*
  Author : Markus Fürer
  Datum  : $Date$
*/

class Pong {

  constructor() {
    const express = require('express');
    const router = express.Router();
    const port = process.env.PONG_SERVICE_PORT || 8020;

    router.get('/ping', (req, res) => {
      console.log(req.url);
      res.send('pong');
    });

    express()
      .use(router)
      .listen(port, () => {
        console.log('listening on port %s.', port);
      });
  }
}

new Pong();
