'use strict';

class API {

  constructor() {
    require('./polyfill');

    // this.bus = require('./modules/bus')(process.env.AMQ62_OPENSHIFT_SERVICE_HOST);
    this.db = require('./oracle');
  }

  addLinks(rows) {
    let links = [];

    rows.map(row => {
      row.links = [];

      for (const [key, value] of Object.entries(row)) {
        // replace variable value
        if (key.indexOf('$') === 0) {
          let link = {
            href: '/' + this.version
          };

          link.rel = key.substring(1);
          link.href += value.replace(/:([^\/']+)/, (match, p1) => this.getEncPath(row[p1]));
          
          delete row[key];
          row.links.push(link);
        }
      }
    });

    return rows;
  }

  getEncPath(path) {
    return path;
  }

  addMetadata(rows, offset, limit) {
    let hasMore = (limit < rows.length);

    if (limit !== Infinity) {
      rows.splice(limit, 1);
    }

    const data = {
      items: rows,
      limit: limit,
      offset: offset,
      hasMore: hasMore,
      rowCount: rows.length,
      links: []
    };

    return data;
  }

  prepareOutput(rows, offset, limit, res) {
    res.set('Cache-Control', 'private, max-age=0, no-store');
    res.set('Expires', '-1');

    rows = this.addLinks(rows);
    return this.addMetadata(rows, offset, limit);
  }

  paramParser() {
    return (req, res, next) => {
      req.offset = parseInt(req.query.offset) || 0;
      req.limit = parseInt(req.query.limit) || Infinity;
      req.persId = req.headers['x-rai-user-id'];

      switch (req.headers['language']) {
        case 'fr':
        case 'fr-CH':
          req.langId = 3;
          break;

        case 'it':
        case 'it-CH':
          req.langId = 4;
          break;

        default:
          req.langId = 1;
          break;
      }

      const message = Object.assign({}, { persId: req.persId, language: req.headers['language'] }, req.params, req.body, req.query);
      // this.bus.publish(func, message);
      // console.info(message);

      next();
    }
  }
}

module.exports = API;
