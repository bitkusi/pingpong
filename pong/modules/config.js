'use strict';

const oracledb = require('oracledb');

const host = process.env.ORACLE_HOST || 'localhost';
const port = process.env.ORACLE_PORT || 32769;
const user = process.env.ORACLE_USER || 'SYSTEM';
const password = process.env.ORACLE_PASSWORD || '123456';
const sid = process.env.ORACLE_SID || 'easybanking';

const Config = {
  connAttrs : {
    user : user,
    password : password,
    connectString : host + ':' + port + '/' + sid
  },

  dbConfig : {
    outFormat : oracledb.OBJECT,
    autoCommit : true,
    xfetchInfo : {
      "HIREDATE" : {
        type : oracledb.STRING
      }, // return the date as a string
      "COMMISSION_PCT" : {
        type : oracledb.DEFAULT
      }
    // override oracledb.fetchAsString and fetch as native type
    }
  }
};

console.log(Config.connAttrs);

module.exports = Config;
