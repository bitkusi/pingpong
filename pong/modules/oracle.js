'use strict';

class Oracle {

  constructor() {
    this.oracledb = require('oracledb');
    this.config = require('./config');
  }

  log(sql, params) {
    if (process.env.NODE_DEBUG == 'oracle') {
      console.log(sql, params);
    }
  }

  execute(sql, params) {
    return this.oracledb.getConnection(this.config.connAttrs).then(connection => {
      this.log(sql, params);

      return connection.execute(sql, params, this.config.dbConfig)
        .then(result => {
          connection.close();
          return result.rows;
        })
        .catch(err => {
          connection.close();
          throw new Error(err);
        });
    });
  }
}

module.exports = new Oracle();