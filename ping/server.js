'use strict';

/*
  Author : Markus Fürer
  Datum  : $Date$
*/

class Ping {

  constructor() {
    const request = require('request');
    const host = process.env.OUTGOING_HOSTNAME || 'localhost';
    const port = process.env.OUTGOING_PORT || 8020;
    const url = 'http://' + host + ':' + port + '/ping';

    setInterval(() => {

      console.log(url);

      request({
        url: url
      },
        (error, response, body) => {
          console.log(error, body);
        });
    }, 5000);
  }
}

new Ping();
